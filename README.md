# Kyle Fontenot
Hi there! I'm a freelance web developer/designer, sci-fi/fantasy novel lover, aspiring video game developer, and just a plain ol' geek. 
My web development focus is in creating SolidJS and React apps and websites with the aspiration of using and contributing to tooling like [SolidJS](https://www.solidjs.com/) and [Astro](https://astro.build/). I love exercising my design muscles, organizing documentation, and using emergent tools. 

### Favorite tools: 
* SolidJS
* Astro
* Next.JS
* CSS modules
* styled-components (well, [Goober](https://goober.rocks/), or [Astroturf](https://4catalyzer.github.io/astroturf/))
* Affinity 

### Looking forward to learning: 
Rust, React Native, Supabase, Redis, more NodeJS, Atom package development, Electron, Godot, Blender, and more drawing skills

### My computer:
* IDE: [VSCode](https://code.visualstudio.com/) (with Vim)
* Browser: [Brave](https://brave.com/)
* Terminal: [Hyper](https://hyper.is/)
* OS: [Pop_OS](https://pop.system76.com/)
* Art: [Affinity Suite](https://affinity.serif.com/en-us/)
* Project Management & Notes: [ClickUp](https://clickup.com/)
